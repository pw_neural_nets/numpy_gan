from DCGAN.models import Generator
import matplotlib.pyplot as plt
import numpy as np
import torch
import torchvision.utils as vutils

lr = 0.0005
betas = (0.5, 0.999)
bs = 64
dropout_p = 0
epochs = 10
image_size = 64
latent_shape = 100

gen_channels_shapes = [latent_shape, 512, 256, 128, 64, 3]

generator_path = 'results/trained_models/DCGAN_generator_10ep_my_64bs.pymodel'
fixed_noise_path = 'results/trained_models/DCGAN_fixed_noise_10ep_my_64bs.pymodel'

generator = Generator(channels_shapes=gen_channels_shapes)

generator.load_state_dict(torch.load(generator_path, map_location=torch.device('cpu')))
fixed_noise = torch.load(fixed_noise_path, map_location=torch.device('cpu'))

plt.figure(figsize=(12, 12))
fake = generator(fixed_noise[37:46]).detach().cpu()
img = vutils.make_grid(fake, nrow=3, padding=2, normalize=True)
plt.imshow(np.transpose(img, (1, 2, 0)))
plt.show()

