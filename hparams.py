from torch import nn

### General
# possible data sources: load_MNIST, load_CELEB, load_cropped_CELEB
loader_type = 'load_CELEB'#'load_MNIST'#
data_path = '../input/celeba-dataset'#'.'#'./data/CELEB'#'../input/cropped-faces-celeba'#'./data'#
disc_save_path = './discriminator_ep{epoch}.pytorch'
gen_save_path = './generator_ep{epoch}.pytorch'
noise_save_path = './noise.pytorch'
imgs_save_path = './training_process_ep{epoch}.jpg'
save_model = True

### Logging
log_interval = 100
save_metric_interval = 200
plot_training_progress = True
img_rows = 4
n_saved_images = int(img_rows**2)

### Loading Normalization
loading_normalization_mean = 0.5
loading_normalization_var = 0.5
in_channels = 3

### Common Parameters
epochs = 10
betas = (0.5, 0.999)
bs = 128
dropout_p = 0
weight_init_var = 0.02
weight_init_mean = 0.
weight_init_mean_bn = 1.
image_size = 32
kernel_size = 4
add_noise = False
noise_intensity = 1.0

### Fixed batchnorm
trainable_bn = True
dataset_stats = {
    'load_MNIST': {'mean': -0.7386, 'var': 5.5634e-05},
    'load_CELEB': {'mean': -0.1289, 'var': 0.0006},
    'load_cropped_CELEB': {'mean': -0.0051, 'var': 0.0003},
}[loader_type]

### exp replay
experience_replay = False
experience_replay_iter_spacing = 500
generator_history_len = 5

### InfoGAN
info_labels = False
cls_loss_coeff = 1.2
code_loss_coeff = 0.1
code_dims = 2
same_label_sampling = True

### CGAN
use_labels = False
n_classes = {
    'load_MNIST': 10,
    'load_CELEB': None,
    'load_cropped_CELEB': None,
}[loader_type] if use_labels or info_labels else 0
label_channel = 1 if use_labels else 0

### SNGAN (without batch norm on disc but not sure)
spectral_norm = False

### WGAN
weight_clipping = 0.01

### WGANGP
grad_penalty = 10.0

### Gen Parameters
latent_shape = 100
gen_lr = 0.0002
gen_channels_shapes = [latent_shape, 512, 256, 128, 64, in_channels] # latent_shape + n_classes
gen_strides = [1, 2, 2, 2, 2]
gen_paddings = [0, 1, 1, 1, 1]
gen_bn = [True, True, True, True, False]
gen_act = nn.ReLU()
gen_out_act = nn.Tanh()
gen_steps = 1
gen_target = 1.0

### Disc Parameters
disc_lr = gen_lr
disc_channels_shapes = [in_channels, 64, 128, 256, 512, 1] # in_channels + label_channel
disc_strides = [2, 2, 2, 2, 1]
disc_paddings = [1, 1, 1, 1, 0]
# SN/WGAN/GP change all to False else [F, T, T,..., F]
disc_bn = [False, True, True, True, False]
disc_act = nn.LeakyReLU(negative_slope=0.2)
disc_out_act = nn.Sigmoid() # lambda x:x #
disc_target = 1.0
disc_steps = 1

# validate correct input shapes
for params in [disc_strides, disc_paddings, disc_bn]:
    assert len(disc_channels_shapes) - 1 == len(params)

for params in [gen_strides, gen_paddings, gen_bn]:
    assert len(gen_channels_shapes) - 1 == len(params)

### Paramter overwrite for mnist
# latent_shape = 100
# in_channels = 1
# image_size = 32
# gen_channels_shapes = [latent_shape + n_classes, 256, 128, 64, in_channels]
# gen_strides = [1, 2, 2, 2]
# gen_paddings = [0, 1, 1, 1]
# gen_bn = [True,  True, True, False]
# disc_channels_shapes = [in_channels + label_channel, 64, 128, 256, 1]
# disc_strides = [2, 2, 2, 1]
# disc_paddings = [1, 1, 1, 0]
# disc_bn = [False, False, False, False]
