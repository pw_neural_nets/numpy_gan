import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal
from torchvision import datasets


class BayesSampler:
    def fit(self, X, y):
        self.classes = sorted(set(y))
        self.cls_gaussians = {}
        self.cls_probs = np.zeros(len(self.classes))
        for cls in self.classes:
            X_cls = X[y == cls]
            flat_X_cls = X_cls.reshape((len(X_cls), -1))
            cls_mean = np.mean(flat_X_cls, axis=0)
            cls_cov = np.cov(flat_X_cls.T)
            self.cls_probs[cls] = np.sum(y == cls)
            self.cls_gaussians[cls] = {'mean': cls_mean, 'cov': cls_cov}
        self.cls_probs = self.cls_probs / np.sum(self.cls_probs)

    def sample_from_yx(self, cls):
        return multivariate_normal.rvs(**self.cls_gaussians[cls])

    def sample(self):
        cls = np.random.choice(self.classes, p=self.cls_probs)
        return self.sample_from_yx(cls)


DATASET = datasets.MNIST(root='./data', download=True)
X = DATASET.train_data.numpy()
y = DATASET.train_labels.numpy()

sampler = BayesSampler()
sampler.fit(X, y)
sample = sampler.sample_from_yx(cls=1)
sample_img = sample.reshape((28, 28)) / sample.max()

max_mean_val = sampler.cls_gaussians[1]['mean'].max()
mean_img = sampler.cls_gaussians[1]['mean'].reshape((28, 28)) / max_mean_val

plt.subplot(1, 2, 1)
plt.imshow(mean_img, cmap='gray')
plt.title('mean')
plt.subplot(1, 2, 2)
plt.imshow(sample_img, cmap='gray')
plt.title('sampled')
plt.show()
