import numpy as np
import matplotlib.pyplot as plt
from torchvision import datasets
from sklearn.mixture import BayesianGaussianMixture as BGM


class BayesSampler:
    def fit(self, X, y):
        self.classes = sorted(set(y))
        self.cls_samplers = {}
        self.cls_probs = np.zeros(len(self.classes))
        for cls in self.classes:
            X_cls = X[y == cls]
            flat_X_cls = X_cls.reshape((len(X_cls), -1))
            sampler = BGM(10)
            sampler.fit(flat_X_cls)
            self.cls_probs[cls] = np.sum(y == cls)
            self.cls_samplers[cls] = sampler
        self.cls_probs = self.cls_probs / np.sum(self.cls_probs)

    def sample(self, cls=None):
        cls = cls if cls else np.random.choice(self.classes, p=self.cls_probs)
        return self.cls_samplers[cls].sample()


sampled_class = 4

DATASET = datasets.MNIST(root='./data', download=True)
X = DATASET.train_data.numpy()
y = DATASET.train_labels.numpy()

sampler = BayesSampler()
sampler.fit(X, y)
sample_dist, cluster = sampler.sample(cls=sampled_class)
sample_img = sample_dist.reshape((28, 28)) / sample_dist.max()

# max_mean_val = sampler.cls_gaussians[1]['mean'].max()
# mean_img = sampler.cls_gaussians[1]['mean'].reshape((28, 28)) / max_mean_val

# plt.subplot(1, 2, 1)
# plt.imshow(mean_img, cmap='gray')
# plt.title('mean')
plt.subplot(1, 2, 2)
plt.imshow(sample_img, cmap='gray')
plt.title(f'sampled class: {sampled_class}, cluster {cluster}')
plt.show()
