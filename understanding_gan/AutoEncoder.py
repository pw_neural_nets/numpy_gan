import torch
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
from torchvision import datasets, transforms
import tqdm


class DenseBlock(nn.Module):
    def __init__(self, in_features, out_features, p):
        super().__init__()
        self.fc = nn.Linear(in_features, out_features)
        self.dropout = nn.Dropout(p=p)
        self.act = nn.ReLU()

    def forward(self, data):
        out = self.fc.forward(data)
        out = self.dropout(out)
        out = self.act(out)
        return out


class Encoder(nn.Module):
    def __init__(self, layer_shapes, dropout_p):
        super().__init__()
        ins = layer_shapes[:-1]
        outs = layer_shapes[1:]
        self.layers = nn.ModuleList([
            DenseBlock(input_shape, output_shape, dropout_p)
            for input_shape, output_shape, in zip(ins, outs)
        ])

    def forward(self, data):
        for layer in self.layers:
            data = layer.forward(data)
        return data


class AutoEncoder(nn.Module):
    def __init__(self, layer_shapes, dropout_p):
        super().__init__()
        encoder_layers, decoder_layers = layer_shapes
        self.encoder = Encoder(encoder_layers, dropout_p)
        self.decoder = Encoder(decoder_layers, dropout_p)

    def forward(self, data):
        encoded_data = self.encoder.forward(data)
        decoded_data = self.decoder.forward(encoded_data)
        return decoded_data, encoded_data


class Experiment:
    def __init__(self, model, loss_fn, dataset, epochs, optimizer):
        self.model = model
        self.loss_fn = loss_fn
        self.dataset = dataset
        self.epochs = epochs
        self.optimizer = optimizer

    def train_model(self):
        self.model.train()
        for epoch in range(self.epochs):
            self.run_epoch()

    def run_epoch(self):
        losses = []
        i = 0
        for x, _ in tqdm.tqdm(self.dataset):
            x = self.flatten(x)
            out, _ = self.model.forward(x)
            loss = self.loss_fn(out, x)
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()
            losses.append(loss.detach().numpy())
        print(np.mean(losses))

    def flatten(self, sample):
        sample = sample.reshape((sample.shape[0], -1))
        return sample


lr = 0.001
bs = 200
dropout_p = 0
epochs = 10

transforms = transforms.Compose([
    transforms.ToTensor(),
])

dataset = datasets.MNIST(root='./data', download=True, transform=transforms)
data_loader = torch.utils.data.DataLoader(
    dataset,
    batch_size=bs,
    shuffle=True,
)

example = dataset[0][0][0]
input_shape = example.reshape(-1).shape[0]

encoder_layers = [input_shape, 300, 200, 50]
decoder_layers = [50, 200, 300, input_shape]
layers = encoder_layers, decoder_layers

model = AutoEncoder(
    dropout_p=dropout_p,
    layer_shapes=layers,
)

optimizer = torch.optim.Adam(model.parameters(), lr)
loss_fn = nn.modules.loss.MSELoss()

exp = Experiment(model, loss_fn, data_loader, epochs, optimizer)
exp.train_model()

example = next(iter(data_loader))[0][0][0].reshape(-1)
out = exp.model.forward(example)[0].reshape((28, 28)).detach().numpy()

plt.subplot(1, 2, 1)
plt.imshow(example.reshape((28, 28)), cmap='gray')
plt.title('example')
plt.subplot(1, 2, 2)
plt.imshow(out, cmap='gray')
plt.title('out')
plt.show()

