import torch
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
from torchvision import datasets, transforms
import tqdm
from torch.distributions import multivariate_normal as mvn
from torch.distributions import bernoulli as bern
from torch.distributions import normal
from torch.nn import functional as F


DIMS = 2


class ToBinary:
    def __call__(self, X):
        X = torch.tensor(X > 0.5, dtype=torch.float32)
        return X


class DenseBlock(nn.Module):
    def __init__(self, in_features, out_features, p):
        super().__init__()
        self.fc = nn.Linear(in_features, out_features)
        self.dropout = nn.Dropout(p=p)
        self.act = nn.ReLU()

    def forward(self, data):
        out = self.fc.forward(data)
        out = self.dropout(out)
        out = self.act(out)
        return out


class Encoder(nn.Module):
    def __init__(self, layer_shapes, dropout_p):
        super().__init__()
        ins = layer_shapes[:-1]
        outs = layer_shapes[1:]
        final_layer = nn.Linear(ins.pop(), outs.pop())
        self.layers = nn.ModuleList([
            DenseBlock(input_shape, output_shape, dropout)
            for input_shape, output_shape, dropout in zip(ins, outs, dropout_p)
        ])
        self.layers.append(final_layer)

    def forward(self, data):
        for layer in self.layers:
            data = layer.forward(data)
        return data


class AutoEncoder(nn.Module):
    def __init__(self, layer_shapes, dropout_p):
        super().__init__()
        encoder_layers, decoder_layers = layer_shapes
        encoder_dropout, decoder_dropout = dropout_p
        self.encoder = Encoder(encoder_layers, encoder_dropout)
        self.decoder = Encoder(decoder_layers, decoder_dropout)
        self.std_act = nn.Softplus()
        self.decoder_act = nn.Sigmoid()

    def decoder_forward(self, latent_parameters):
        decoded_pixel_logits = self.decoder.forward(latent_parameters)
        pixel_probabilities = self.decoder_act(decoded_pixel_logits)

        prior_pred_dist = bern.Bernoulli(logits=decoded_pixel_logits)
        sampled_pixels = prior_pred_dist.sample()
        return pixel_probabilities, sampled_pixels

    def forward(self, data):
        gaussian_parameters = self.encoder.forward(data)
        mean, std = torch.split(gaussian_parameters, DIMS, dim=1)
        # let it DCGAN full cov matrix
        # mean = self.mean_act(mean)

        std = self.std_act(std) + 1e-6

        sample = mvn.MultivariateNormal(loc=mean, covariance_matrix=torch.diag_embed(std**2)).rsample()
        # sample = normal.Normal(loc=mean, scale=std).rsample()
        decoded_pixel_logits = self.decoder.forward(sample)
        pixel_probabilities = self.decoder_act(decoded_pixel_logits)
        return pixel_probabilities, mean, std


class Experiment:
    def __init__(self, model, loss_fn, dataset, epochs, optimizer):
        self.model = model
        self.loss_fn = loss_fn
        self.dataset = dataset
        self.epochs = epochs
        self.optimizer = optimizer

    def train_model(self):
        self.model.train()
        for epoch in range(self.epochs):
            self.run_epoch()

    def run_epoch(self):
        losses = []
        for x, _ in tqdm.tqdm(self.dataset):
            x = self.flatten(x)
            out = self.model.forward(x)
            loss = self.loss_fn(out, x)
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()
            losses.append(loss.detach().numpy())
        print(np.mean(losses))

    def flatten(self, sample):
        sample = sample.reshape((sample.shape[0], -1))
        return sample


class ELBO_Loss(nn.Module):
    def __init__(self, bs):
        super().__init__()
        target_mean = torch.zeros(bs, DIMS)
        target_cov = torch.eye(DIMS).repeat(bs, 1, 1)
        self.standard_normal = mvn.MultivariateNormal(
            loc=target_mean,
            covariance_matrix=target_cov,
        )
        self.mse = nn.modules.loss.MSELoss(reduction='sum')
        self.bce = torch.nn.BCELoss(reduction='sum')
        self.iter = 0

    def forward(self, predictions, target):
        pred_sample, mean, std = predictions
        pred_dist = mvn.MultivariateNormal(loc=mean, covariance_matrix=torch.diag_embed(std**2))
        kl_div = torch.distributions.kl.kl_divergence(pred_dist, self.standard_normal)

        kl_div = torch.sum(kl_div)
        mse_loss = self.mse(pred_sample, target)
        loss = mse_loss + kl_div

        if not self.iter % 100: print(f'kl: {torch.mean(kl_div)}, mse: {torch.mean(mse_loss)}, ELBO: {loss}')
        self.iter += 1
        return loss


lr = 0.001
bs = 128
encoder_dropout_p = [0] * 4 #[0.2, 0.15, 0.1, 0.0, 0.0]
decoder_dropout_p = [0] * 4 #[0.0, 0.0, 0., 0., 0.0]
dropout_p = encoder_dropout_p, decoder_dropout_p
epochs = 200


transforms = transforms.Compose([
    transforms.ToTensor(),
    # ToBinary(),
])

dataset = datasets.MNIST(root='./data', download=True, transform=transforms)
data_loader = torch.utils.data.DataLoader(
    dataset,
    batch_size=bs,
    shuffle=True,
    drop_last=True,
)

example = dataset[0][0][0]
input_shape = example.reshape(-1).shape[0]

encoder_layers = [input_shape, 200, 100, 2*DIMS]
decoder_layers = [DIMS, 100, 200, input_shape]
layers = encoder_layers, decoder_layers

model = AutoEncoder(
    dropout_p=dropout_p,
    layer_shapes=layers,
)

optimizer = torch.optim.Adam(model.parameters(), lr)
#optimizer = torch.optim.SGD(DCGAN.parameters(), lr, momentum=0.9)
loss_fn = ELBO_Loss(bs=bs)

exp = Experiment(model, loss_fn, data_loader, epochs, optimizer)
try:
    exp.train_model()
except:
    import pdb;pdb.set_trace()

# fig, axs = plt.subplots(10, 10)
# for x in range(10):
#     for y in range(10):
#         sample = torch.FloatTensor([(x-5)/5, (y-5)/5])
#         sampled_img = exp.DCGAN.decoder.forward(sample).detach().reshape((28, 28))
#         axs[x, y].imshow(sampled_img, cmap='gray')
#         axs[x, y].set_axis_off()
# plt.subplots_adjust()
#
# plt.show()
#
# import pdb;
# pdb.set_trace()


plt.imshow(example, cmap='gray')
plt.show()
sampled_img = exp.model.forward(example.flatten().unsqueeze(dim=0))[0].detach().reshape((28, 28))
sampled_img = exp.model.decoder_act(sampled_img)
plt.imshow(sampled_img, cmap='gray')
plt.show()

n = 20 # number of images per side
x_values = np.linspace(-3, 3, n)
y_values = np.linspace(-3, 3, n)
image = np.empty((28 * n, 28 * n))

k = 0
for i, x in enumerate(x_values):
    for j, y in enumerate(y_values):
        sample = torch.FloatTensor([x, y])
        sample = sample.unsqueeze(dim=0)
        # sampled_img = exp.DCGAN.decoder_forward(torch.FloatTensor([1.] * DIMS))[0].detach().reshape((28, 28))
        # plt.imshow(sampled_img, cmap='gray')
        # plt.show()
        sampled_img = exp.model.decoder_forward(sample)[1][0].detach().reshape((28, 28))

        k += 1
        # convert from NxD == 1 x 784 --> 28 x 28
        image[(n - i - 1) * 28:(n - i) * 28, j * 28:(j + 1) * 28] = sampled_img
plt.imshow(image, cmap='gray')
plt.show()

n = 20 # number of images per side
x_values = np.linspace(-3, 3, n)
y_values = np.linspace(-3, 3, n)
image = np.empty((28 * n, 28 * n))

k = 0
for i, x in enumerate(x_values):
    for j, y in enumerate(y_values):
        sample = torch.FloatTensor([x, y])
        mean_img = exp.model.decoder_forward(sample)[0].detach().reshape((28, 28))
        k += 1
        # convert from NxD == 1 x 784 --> 28 x 28
        image[(n - i - 1) * 28:(n - i) * 28, j * 28:(j + 1) * 28] = mean_img
plt.imshow(image, cmap='gray')
plt.show()
