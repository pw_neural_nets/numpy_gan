SEED = 420
import random
import numpy as np
import torch
import torchvision.utils as vutils
import matplotlib.pyplot as plt
import torchvision.transforms as transforms
from torchvision import datasets

from gan_utils import data_loading
import hparams
from DCGAN import models
from DCGAN import pipeline
from DCGAN import losses

np.random.seed(SEED)
random.seed(SEED)
torch.manual_seed(SEED)

def calculate_img_avg(data_loader):
    n = 0
    cum_sum = 0
    cum_sum_sq = 0
    for img, _ in data_loader:
        print(n)
        cum_sum += img.mean()
        cum_sum_sq += img.mean()**2
        n += 1

    mean = cum_sum / n
    var = 1/n * (cum_sum_sq - (cum_sum**2) / n)
    return mean, var

if __name__ == "__main__":
    device = torch.device("cuda:0" if (torch.cuda.is_available() and 1 > 0) else "cpu")

    data_loader = data_loading.get_data_loader(
        loader_type=hparams.loader_type,
        data_path=hparams.data_path,
        bs=hparams.bs,
        image_size=hparams.image_size
    )

    # calculate_img_avg(data_loader)

    generator = models.Generator(channels_shapes=hparams.gen_channels_shapes).to(device)
    discriminator = models.Discriminator(channels_shapes=hparams.disc_channels_shapes).to(device)

    # should the weight init be the same as for DCGAN when Wasserstein loss is used?
    generator.apply(models.weights_init)
    discriminator.apply(models.weights_init)

    gen_optimizer = torch.optim.RMSprop(generator.parameters(), lr=hparams.gen_lr)
    disc_optimizer = torch.optim.RMSprop(discriminator.parameters(), lr=hparams.disc_lr)

    gen_loss_fn = losses.WassersteinGeneratorLoss()
    disc_loss_fn = losses.WassersteinDiscriminatorLoss()

    wgan = pipeline.WGAN(
        generator=generator,
        discriminator=discriminator,
        gen_loss_fn=gen_loss_fn,
        disc_loss_fn=disc_loss_fn,
        gen_optimizer=gen_optimizer,
        disc_optimizer=disc_optimizer,
        data_loader=data_loader,
        device=device,
    )

    wgan.train_model()

    plt.figure(figsize=(10, 5))
    plt.title("Generator and Discriminator Loss During Training")
    plt.plot(wgan.gen_losses, label="G")
    plt.plot(wgan.disc_losses, label="D")
    plt.xlabel("iterations")
    plt.ylabel("Loss")
    plt.legend()
    plt.show()

    # Grab a batch of real images from the dataloader
    real_batch = next(iter(data_loader))

    # Plot the real images
    plt.figure(figsize=(15, 15))
    plt.subplot(1, 2, 1)
    plt.axis("off")
    plt.title("Real Images")
    plt.imshow(
        np.transpose(vutils.make_grid(
            real_batch[0].to(device)[:128],
            padding=5,
            normalize=True
        ).cpu(), (1, 2, 0)))

    # Plot the fake images from the last epoch
    plt.subplot(1, 2, 2)
    plt.axis("off")
    plt.title("Fake Images")
    plt.imshow(np.transpose(wgan.img_list[-5], (1, 2, 0)))
    plt.show()
