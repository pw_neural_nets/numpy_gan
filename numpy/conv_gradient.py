import numpy as np


def conv_forward_naive(x, w, b, conv_param):
    stride = conv_param['stride']
    pad = conv_param['pad']
    padded_X = np.pad(x, [(0, 0), (0, 0), (pad, pad), (pad, pad)], 'constant', constant_values=0)

    N, C, H, W = padded_X.shape
    F, Cw, HH, WW = w.shape
    Fb = b.shape[0]

    assert C == Cw, f'number of channels is different for X {C} and W {Cw}'
    assert F == Fb, f'number of filters is different for W {F} and b {Fb}'

    new_H = 1 + (H - HH) // stride
    new_W = 1 + (W - WW) // stride

    new_X = np.zeros((N, F, new_H, new_W))

    for n in range(N):
        for f in range(F):
            new_X[n, f, ...] += b[f]
            for c in range(C):
                for new_h in range(new_H):
                    for new_w in range(new_W):
                        for hh in range(HH):
                            for ww in range(WW):
                                new_X[n, f, new_w, new_h] += w[f, c, ww, hh] * padded_X[
                                    n, c, new_w * stride + ww, new_h * stride + hh]

    return new_X, (x, w, b, conv_param)


def conv_backward_naive(dout, cache):
    x, w, b, conv_param = cache
    stride, pad = conv_param['stride'], conv_param['pad']

    N, C, H, W = x.shape
    F, C, HH, WW = w.shape

    w_f = np.flip(w, (2, 3))
    padded_x = np.pad(x, [(0,), (0,), (pad,), (pad,)], 'constant', constant_values=0)

    N, F, H_out, W_out = dout.shape
    dw = np.zeros((F, C, HH, WW))

    db = np.sum(dout, axis=(0, 2, 3))

    for n in range(N):
        for f in range(F):
            for c in range(C):
                for h in range(H_out):
                    for w in range(W_out):
                        for hh in range(HH):
                            for ww in range(WW):
                                dw[f, c, hh, ww] += dout[n, f, h, w] * padded_x[
                                    n, c, stride * h + hh, stride * w + ww]

    return 0, dw, db

def ReLU(x):
    mask = (x > 0) * 1.0
    return mask * x

def d_ReLU(x):
    mask = (x > 0) * 1.0
    return mask

lr = 0.001
np.random.seed(231)
x = np.random.randn(4, 3, 5, 5)
w1 = np.random.randn(2, 3, 3, 3)
b1 = np.random.randn(2,)
y = np.random.randn(4, 2, 5, 5)
conv_param = {'stride': 1, 'pad': 1}

for i in range(100):
    out1, cache1 = conv_forward_naive(x, w1, b1, conv_param)
    out2 = ReLU(out1)

    dout = y - out1
    cost = 1/4 * np.sum((dout**2)**1/2)
    print(f'dout: {cost}')
    print(f'dout: {np.sum(dout)}')

    _, dw1, db1 = conv_backward_naive(dout, cache1)
    dw1 = dout * d_ReLU(out2) * dw1
    db1 = dout * d_ReLU(out2) * db1

    w1 -= lr * dw1
    b1 -= lr * db1

