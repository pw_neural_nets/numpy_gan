import torch
import torchvision.utils as vutils
import matplotlib.pyplot as plt
import numpy as np
import hparams

#TODO
# implement experience replay
# a) cache old samples in memory (works fast but memory extensive)
# b) load saved model and run it (slow but does not require lots of memory)
# make sure it works correcrly for WGAN and WGANGP
# FIX IT. WHEN RUN FOR 500 STEPS NORMAL MODEL RETURN FACES AND THIS RETURNS NOISE
# (WITH EXTRA DATA ADDED AFTER 1000 STEPS SO THE OUTPUT SHOULD BE THE SAME)


class DCGAN:
    """
            /---------------------<<<<<----------------------\
    1. generator ---> fake_img                            gen_loss
                          +    --->   discriminator  --->    +
                         img                |             disc_loss
                                            ------<<<<<------/
    """
    def __init__(
            self,
            generator,
            discriminator,
            gen_loss_fn,
            disc_loss_fn,
            gen_optimizer,
            disc_optimizer,
            data_loader,
            device,
            disc_lr_scheduler=None,
            gen_lr_scheduler=None,
    ):
        self.generator = generator
        self.discriminator = discriminator
        self.gen_loss_fn = gen_loss_fn
        self.gen_optimizer = gen_optimizer
        self.disc_loss_fn = disc_loss_fn
        self.disc_optimizer = disc_optimizer
        self.latent_shape = hparams.latent_shape
        self.gen_lr_scheduler = gen_lr_scheduler
        self.disc_lr_scheduler = disc_lr_scheduler

        self.data_loader = data_loader
        self.epochs = hparams.epochs
        self.log_interval = hparams.log_interval
        self.counter = 0

        self.img_list = []
        self.gen_losses = []
        self.disc_losses = []
        self.disc_target = hparams.disc_target
        self.gen_target = hparams.gen_target
        self.gen_steps = hparams.gen_steps
        self.disc_steps = hparams.disc_steps
        self.device = device

        self.fixed_noise = None
        self.img_white_noise = None

        self.experience_buffer = []

    def train_model(self):
        """latent_features should come from 100 dim uniform distribution [0, 1)"""
        self.generator.train()
        self.discriminator.train()
        for epoch in range(self.epochs):
            self.run_epoch(epoch)
            if hparams.save_model: self.save_model(epoch)

    def run_epoch(self, epoch):
        self.counter = 0
        for sample in self.data_loader:
            img, label = sample

            img = img.to(self.device)
            label = label.to(self.device)

            logging_data = self._run_epoch(img, label)

            self.log_loss(epoch, logging_data)
            self.save_img(epoch)

            if hparams.plot_training_progress: self.plot()
            if self.counter % self.disc_steps == 0:
                if self.gen_lr_scheduler: self.gen_lr_scheduler.step()
                if self.disc_lr_scheduler: self.disc_lr_scheduler.step()
            self.counter += 1

    def _run_epoch(self, img, label):
        latent_features = self.generate_latent_features(img.shape[0], label=label)
        img = self.process_real_img(img, label)
        gen_img = self.generate_img(latent_features, label=label, extend_history=True)

        log_disc_data = self.forward_disc(
            img=img, gen_img=gen_img)
        log_gen_data = self.forward_gen(
            gen_img=gen_img, latent_features=latent_features)
        return log_disc_data, log_gen_data

    def forward_disc(self, img, gen_img, **kwargs):
        self.discriminator.zero_grad()
        pred_real_img = self.discriminator(img).reshape(-1)
        label_real_img = torch.ones(pred_real_img.shape[0], device=self.device) * self.disc_target
        real_img_loss = self.disc_loss_fn(pred_real_img, label_real_img)
        real_img_loss.backward()

        pred_gen_img = self.discriminator(gen_img.detach()).reshape(-1)
        label_gen_img = torch.zeros(pred_gen_img.shape[0], device=self.device)
        gen_img_loss = self.disc_loss_fn(pred_gen_img, label_gen_img)
        gen_img_loss.backward()

        D_x = pred_real_img.mean().item()
        D_G_z1 = pred_gen_img.mean().item()

        disc_loss = gen_img_loss + real_img_loss

        if self.counter % self.disc_steps == 0:
            self.disc_optimizer.step()
            self.disc_losses.append(disc_loss.item())

        return D_x, D_G_z1

    def forward_gen(self, gen_img, latent_features, **kwargs):
        D_G_z2 = np.nan
        if self.counter % self.disc_steps == 0:
            for _ in range(self.gen_steps):
                self.generator.zero_grad()
                pred_gen_img = self.discriminator(gen_img).reshape(-1)
                label_gen_img = torch.ones(pred_gen_img.shape[0], device=self.device) * self.gen_target
                gen_loss = self.gen_loss_fn(pred_gen_img, label_gen_img)
                gen_loss.backward()

                D_G_z2 = pred_gen_img.mean().item()
                self.gen_optimizer.step()
                if self.gen_steps > 1:
                    gen_img = self.generate_img(latent_features)

                self.gen_losses.append(gen_loss.item())
        return D_G_z2

    def generate_img(self, latent_features, label=None, extend_history=False):
        try:
            img = self.generator(latent_features)
        except:
            import pdb;
            pdb.set_trace()
        if hparams.add_noise:
            img = img + hparams.noise_intensity * self.img_white_noise
        if hparams.experience_replay:
            img = self.add_old_generator_iterations(img, extend_history)
        return img

    def add_old_generator_iterations(self, img, extend_history):
        if self.is_extend_experience_buffer(extend_history):
            self.experience_buffer.insert(0, img.detach())
            if self.is_experience_buffer_overflowed():
                self.experience_buffer.pop()
        extended_img = torch.cat([img] + self.experience_buffer)
        return extended_img

    def process_real_img(self, img, label, **kwargs):
        img = self.add_white_noise(img)
        return img

    def add_white_noise(self, img):
        if hparams.add_noise:
            self.img_white_noise = torch.randn(
                img.shape,
                device=self.device,
                requires_grad=False
            ).clamp_(-1, 1)
            img = img + hparams.noise_intensity * self.img_white_noise
        return img

    def is_experience_buffer_overflowed(self):
        return len(self.experience_buffer) > hparams.generator_history_len

    def is_extend_experience_buffer(self, extend_history):
        return self.counter % hparams.experience_replay_iter_spacing == 0 and extend_history

    def log_loss(self, epoch, logging_data):
        log_disc_data, log_gen_data = logging_data
        D_x, D_G_z1 = log_disc_data
        D_G_z2 = log_gen_data

        if self.counter % hparams.log_interval == 0:
            print('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                  % (epoch, self.epochs, self.counter, len(self.data_loader),
                     self.disc_losses[-1], self.gen_losses[-1], D_x, D_G_z1, D_G_z2))

    def generate_latent_features(self, batch_size, **kwargs):
        return torch.randn(batch_size, self.latent_shape, 1, 1, device=self.device)

    def save_img(self, epoch):
        if self.fixed_noise is None:
            self.fixed_noise = self.generate_latent_features(hparams.n_saved_images)

        if (self.counter % hparams.save_metric_interval == 0) or (
                (epoch == self.epochs - 1) and (self.counter == len(self.data_loader) - 1)):
            with torch.no_grad():
                fake = self.generator(self.fixed_noise).detach().cpu()
            self.img_list.append(np.transpose(vutils.make_grid(
                fake, padding=2, nrow=hparams.img_rows, normalize=True), (1, 2, 0)))

    def save_model(self, epoch):
        torch.save(self.generator.state_dict(), hparams.gen_save_path.format(epoch=epoch))
        torch.save(self.discriminator.state_dict(), hparams.disc_save_path.format(epoch=epoch))
        torch.save(self.fixed_noise, hparams.noise_save_path)
        plt.imshow(self.img_list[-1])
        plt.savefig(hparams.imgs_save_path.format(epoch=epoch))

    def plot(self):
        if self.counter % hparams.save_metric_interval == 0:
            real_batch = next(iter(self.data_loader))

            plt.figure(figsize=(15, 15))
            plt.subplot(1, 2, 1)
            plt.axis("off")
            plt.title("Real Images")
            plt.imshow(np.transpose(vutils.make_grid(
                real_batch[0].to(self.device)[:hparams.n_saved_images],
                nrow=hparams.img_rows,
                padding=2,
                normalize=True
            ).cpu(), (1, 2, 0)))

            plt.subplot(1, 2, 2)
            plt.axis("off")
            plt.title("Fake Images")
            plt.imshow(self.img_list[-1])
            plt.show()


class WGAN(DCGAN):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.weight_clipping = hparams.weight_clipping

    def forward_disc(self, img, fake_img, **kwargs):
        self.discriminator.zero_grad()
        fake_img = fake_img.detach()

        disc_loss = -torch.mean(self.discriminator(img)) + torch.mean(self.discriminator(fake_img))
        disc_loss.backward()
        self.disc_optimizer.step()

        self.clip_discriminator_weights()

        if self.counter % self.disc_steps == 0:
            self.disc_losses.append(disc_loss)

        return disc_loss, fake_img

    def forward_gen(self, gen_img, **kwargs):
        gen_loss = np.nan
        if self.counter % self.disc_steps == 0:
            self.generator.zero_grad()
            gen_loss = -torch.mean(self.discriminator(gen_img))

            gen_loss.backward()
            self.gen_optimizer.step()

            self.gen_losses.append(gen_loss)
        return gen_loss

    def clip_discriminator_weights(self):
        for p in self.discriminator.parameters():
            p.data.clamp_(-1 * self.weight_clipping, self.weight_clipping)

    def log_loss(self, epoch, logging_data):
        disc_loss, gen_loss = logging_data
        if self.counter % self.disc_steps == 0:
            print(
                "[%d/%d] [%d/%d] [D loss: %f] [G loss: %f]"
                % (epoch, self.epochs, self.counter % len(self.data_loader),
                   len(self.data_loader), disc_loss, gen_loss)
            )


class WGANGP(DCGAN):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.grad_penalty_coefficient = hparams.grad_penalty
        self.disc_steps = hparams.disc_steps

    def get_gradient_penalty(self, fake_img, real_img):
        alpha = torch.randn(real_img.shape[0], 1, 1, 1, requires_grad=True, device=self.device)

        img_mix = alpha * real_img + (1 - alpha) * fake_img
        pred = self.discriminator(img_mix)

        labels = torch.ones(real_img.shape[0], 1, 1, 1, requires_grad=False, device=self.device)

        gradient = torch.autograd.grad(
            outputs=pred,
            inputs=img_mix,
            grad_outputs=labels,
            retain_graph=True,
            create_graph=True,
            only_inputs=True,
        )[0]

        gradient = gradient.view(gradient.size(0), -1)
        grad_penalty = ((gradient.norm(2, dim=1) - 1)**2).mean()
        return grad_penalty

    def forward_disc(self, img, fake_img, **kwargs):
        self.discriminator.zero_grad()
        fake_img = fake_img.detach()
        current_iteration_samples = self.get_current_iteration_samples(fake_img=fake_img, batch_size=img.shape[0])
        grad_penalty = self.get_gradient_penalty(fake_img=current_iteration_samples, real_img=img)

        adversarial_loss = -torch.mean(self.discriminator(img)) + torch.mean(self.discriminator(fake_img))
        disc_loss = adversarial_loss + self.grad_penalty_coefficient * grad_penalty
        disc_loss.backward()
        self.disc_optimizer.step()

        if self.counter % self.disc_steps == 0:
            self.disc_losses.append(disc_loss)

        return disc_loss, fake_img

    def get_current_iteration_samples(self, fake_img, batch_size):
        return fake_img[:batch_size]

    def forward_gen(self, gen_img, **kwargs):
        gen_loss = np.nan
        if self.counter % self.disc_steps == 0:
            self.generator.zero_grad()
            #fake_img = self.generate_img(latent_features) when image is not generated after zero grad is that a probelm?
            gen_loss = -torch.mean(self.discriminator(gen_img))

            gen_loss.backward()
            self.gen_optimizer.step()

            self.gen_losses.append(gen_loss)
        return gen_loss

    def log_loss(self, epoch, logging_data):
        disc_loss, gen_loss = logging_data
        if self.counter % self.disc_steps == 0:
            print(
                "[%d/%d] [%d/%d] [D loss: %f] [G loss: %f]"
                % (epoch, self.epochs, self.counter % len(self.data_loader),
                   len(self.data_loader), disc_loss, gen_loss)
            )


class CGAN(DCGAN):
    # 1. linear layer vs channel class encoding
    def process_real_img(self, img, label, **kwargs):
        img = super().process_real_img(img, label)
        img = self.add_label_to_img(img, label)
        return img

    def generate_img(self, latent_features, label=None, extend_history=False):
        img = self.generator(latent_features)
        if hparams.add_noise:
            img = img + hparams.noise_intensity * self.img_white_noise
        img = self.add_label_to_img(img, labels=label)
        if hparams.experience_replay:
            img = self.add_old_generator_iterations(img, extend_history)
        return img

    def generate_latent_features(self, batch_size, label=None):
        if label is None:
            label = (hparams.n_classes * torch.rand(batch_size, device=self.device)).long()
        latent_features = torch.randn(batch_size, self.latent_shape, 1, 1, device=self.device)
        latent_features = self.add_label_to_latent_features(latent_features, label)
        return latent_features

    def add_label_to_img(self, img, labels):
        """ create extra channel with 1/n filled with ones per class """
        label_channel = torch.zeros((img.shape[0], 1, *img.shape[2:]), device=self.device)
        label_channel = label_channel.view(label_channel.shape[0], -1)
        ones_per_class = label_channel.shape[-1] // hparams.n_classes
        start = ones_per_class * labels
        for i, label, s in zip(range(label_channel.shape[0]), labels, start):
            label_channel[i, s:s+ones_per_class] = 1
        label_channel = label_channel.reshape(img.shape[0], 1, *img.shape[2:])
        img = torch.cat([img, label_channel], dim=1)
        return img

    def add_label_to_latent_features(self, latent_features, label):
        one_hot_labels = torch.sparse.torch.eye(hparams.n_classes, device=self.device).index_select(dim=0, index=label)
        one_hot_labels = one_hot_labels.reshape((*one_hot_labels.shape, 1, 1))
        latent_features = torch.cat([latent_features, one_hot_labels], dim=1)
        return latent_features


class InfoGAN(DCGAN):
    # 1. swap order disc and gen? does it matter in any way?
    # 2. resample for gen, disc and info VS use the same sample per iteration
    def __init__(self, info_optimizer, code_loss, cls_loss, **kwargs):
        super().__init__(**kwargs)
        self.info_optimizer = info_optimizer
        self.code_loss = code_loss
        self.cls_loss = cls_loss
        self.info_losses = []

    def _run_epoch(self, img, label):
        img = self.process_real_img(img, label=None)
        gt_gen_img_cls = label if hparams.same_label_sampling else (hparams.n_classes * torch.rand(img.shape[0], device=self.device)).long()
        gt_code = self.generate_code(batch_size=img.shape[0])

        latent_features = self.generate_latent_features(img.shape[0], label=gt_gen_img_cls, code=gt_code)
        gen_img = self.generate_img(latent_features, extend_history=True)

        log_gen_data = self.forward_gen(
            gen_img=gen_img, latent_features=latent_features)

        log_disc_data = self.forward_disc(
            img=img, gen_img=gen_img)

        info_loss = self.get_info_loss(latent_features, gt_gen_img_cls=gt_gen_img_cls, gt_code=gt_code)

        return log_disc_data, log_gen_data, info_loss

    def get_info_loss(self, latent_features, gt_gen_img_cls, gt_code):
        self.info_optimizer.zero_grad()
        # make sure gt_codes and gt_gen_img_cls have correct size for their respective loss functions
        fake_img = self.generate_img(latent_features)
        _, pred_img_cls, pred_code = self.discriminator(fake_img, return_info_probs=True)

        info_loss = (hparams.cls_loss_coeff * self.cls_loss(pred_img_cls, gt_gen_img_cls) +
                     hparams.code_loss_coeff * self.code_loss(pred_code, gt_code.squeeze()))
        info_loss.backward()
        self.info_optimizer.step()

        if self.counter % self.disc_steps == 0:
            self.info_losses.append(info_loss)

        return info_loss

    def generate_latent_features(self, batch_size, label=None, code=None):
        if label is None:
            label = (hparams.n_classes * torch.rand(batch_size, device=self.device)).long()
        if code is None:
            code = self.generate_code(batch_size)
        latent_features = torch.randn(batch_size, self.latent_shape, 1, 1, device=self.device)
        latent_features = self.add_label_to_latent_features(latent_features, label=label)
        latent_features = torch.cat([latent_features, code], dim=1)
        return latent_features

    def add_label_to_latent_features(self, latent_features, label):
        one_hot_labels = torch.sparse.torch.eye(hparams.n_classes, device=self.device).index_select(dim=0, index=label)
        one_hot_labels = one_hot_labels.reshape((*one_hot_labels.shape, 1, 1))
        latent_features = torch.cat([latent_features, one_hot_labels], dim=1)
        return latent_features

    def generate_code(self, batch_size):
        return 2 * torch.rand(batch_size, hparams.code_dims, 1, 1, device=self.device) - 1

    def log_loss(self, epoch, logging_data):
        log_disc_data, log_gen_data, info_loss = logging_data
        D_x, D_G_z1 = log_disc_data
        D_G_z2 = log_gen_data

        if self.counter % hparams.log_interval == 0:
            print('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f\t[I loss: %f]\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                  % (epoch, self.epochs, self.counter, len(self.data_loader),
                     self.disc_losses[-1], self.gen_losses[-1], self.info_losses[-1], D_x, D_G_z1, D_G_z2))
