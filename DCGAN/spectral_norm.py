import torch
from torch import nn


class SpectralNorm(nn.Module):
    def __init__(self, module):
        super().__init__()
        self.module = module

        height = self.module.weight.shape[0]
        width = self.module.weight.view(height, -1).shape[1]

        self.register_buffer('u', torch.randn(height, requires_grad=False))
        self.register_buffer('v', torch.randn(width, requires_grad=False))

    def forward(self, *args, **kwargs):
        self.normalize_weights()
        out = self.module.forward(*args, **kwargs)
        return out

    def normalize_weights(self):
        w = self.module.weight.data.view(self.u.shape[0], -1)

        self.v = self.l2_norm(torch.matmul(w.T, self.u))
        self.u = self.l2_norm(torch.matmul(w, self.v))

        sigma_w = self.u.T @ torch.mv(w, self.v)
        self.module.weight.data /= sigma_w

    def l2_norm(self, matrix):
        normalized_matrix = matrix / (matrix.norm(2) + 1e-12)
        return normalized_matrix
