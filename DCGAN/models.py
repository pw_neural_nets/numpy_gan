from torch import nn

from DCGAN import spectral_norm
import hparams


def weights_init(model):
    classname = model.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(model.weight.data, hparams.weight_init_mean, hparams.weight_init_var)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(model.weight.data, hparams.weight_init_mean_bn, hparams.weight_init_var)
        nn.init.normal_(model.bias.data, 0)


class BatchFixedNorm(nn.Module):
    def __init__(self):
        super().__init__()
        self.mean = hparams.dataset_stats['mean']
        self.var = hparams.dataset_stats['var']

    def forward(self, input_):
        return (input_ - self.mean) / (self.var**(1/2))


class BasicBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding, act, frac_conv=False, bn=True, is_spectral_norm=False):
        super().__init__()
        conv_cls = nn.ConvTranspose2d if frac_conv else nn.Conv2d
        self.conv = conv_cls(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
            bias=False,
        )
        self.conv = spectral_norm.SpectralNorm(self.conv) if is_spectral_norm else self.conv
        self.bn = self.get_batch_norm(bn, out_channels)
        self.act = act

    def get_batch_norm(self, batch_norm, out_channels):
        if batch_norm:
            if hparams.trainable_bn:
                bn = nn.BatchNorm2d(num_features=out_channels)
            else:
                bn = BatchFixedNorm()
        else:
            bn = None
        return bn

    def forward(self, X):
        out = self.conv(X)
        if self.bn: out = self.bn(out)
        out = self.act(out)
        return out


class Discriminator(nn.Module):
    def __init__(self, channels_shapes, kernel_size=hparams.kernel_size,
                 strides=hparams.disc_strides, paddings=hparams.disc_paddings,
                 act=hparams.disc_act, out_act=hparams.disc_out_act, batch_norms=hparams.disc_bn):
        super().__init__()
        ins, outs = channels_shapes[:-1], channels_shapes[1:]
        acts = (len(ins)-1) * [act] + [out_act]

        self.layers = nn.ModuleList([
            BasicBlock(input_shape, output_shape, is_spectral_norm=hparams.spectral_norm,
                       kernel_size=kernel_size, stride=stride, padding=padding, act=act, bn=bn)
            for input_shape, output_shape, stride, padding, act, bn
            in zip(ins, outs, strides, paddings, acts, batch_norms)
        ])

    def forward(self, X):
        for layer in self.layers:
            X = layer.forward(X)
        return X


class InfoDiscriminator(Discriminator):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        final_conv = self.layers[-1]
        self.layers = self.layers[:-1]
        input_shape = int(final_conv.conv.in_channels * (hparams.image_size / 2**(len(self.layers)))**2)
        self.avd_net = final_conv # BCE
        self.cls_fc = nn.Linear(input_shape, hparams.n_classes) # CE
        self.code_fc = nn.Linear(input_shape, hparams.code_dims) # MSE
        self.sig = nn.Sigmoid()
        self.softmax = nn.Softmax(dim=1)

    def forward(self, X, return_info_probs=False):
        for layer in self.layers:
            X = layer.forward(X)
        avd_out = self.avd_net(X)
        cls_out = self.softmax(self.cls_fc(X.view(X.shape[0], -1)))
        code_out = self.sig(self.code_fc(X.view(X.shape[0], -1)))
        if return_info_probs:
            return avd_out, cls_out, code_out
        else:
            return avd_out


class Generator(nn.Module):
    def __init__(self, channels_shapes, kernel_size=hparams.kernel_size,
                 strides=hparams.gen_strides, paddings=hparams.gen_paddings,
                 act=hparams.gen_act, out_act=hparams.gen_out_act, batch_norms=hparams.gen_bn):
        super().__init__()
        ins, outs = channels_shapes[:-1], channels_shapes[1:]
        acts = (len(ins)-1) * [act] + [out_act]

        self.layers = nn.ModuleList([
            BasicBlock(input_shape, output_shape, frac_conv=True,
                       kernel_size=kernel_size, stride=stride, padding=padding, act=act, bn=bn)
            for input_shape, output_shape, stride, padding, act, bn in
            zip(ins, outs, strides, paddings, acts, batch_norms)
        ])

    def forward(self, X):
        for layer in self.layers:
            X = layer.forward(X)
        return X


class InfoGenerator(Generator):
    def __init__(self, **kwargs):
        kwargs['channels_shapes'][0] += hparams.code_dims
        super().__init__(**kwargs)
