# import cv2
#
# face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#
# img = cv2.imread('../data/CELEB/img_align_celeba/000001.jpg')
# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#
# faces = face_cascade.detectMultiScale(gray, 1.3, 3)
# for (x, y, w, h) in faces:
#     img = cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
#     roi_gray = gray[y:y + h, x:x + w]
#     roi_color = img[y:y + h, x:x + w]
#
# cv2.imshow('img', img)
# cv2.waitKey(0)
# cv2.destroyAllWindows()
# example of extracting and resizing faces into a new dataset
from os import listdir
from numpy import asarray
from numpy import savez_compressed
from PIL import Image
from mtcnn.mtcnn import MTCNN
from matplotlib import pyplot


# load an image as an rgb numpy array
def load_image(filename):
    # load image from file
    image = Image.open(filename)
    # convert to RGB, if needed
    image = image.convert('RGB')
    # convert to array
    pixels = asarray(image)
    return pixels


# extract the face from a loaded image and resize
def extract_face(model, pixels, required_size=(80, 80)):
    # detect face in the image
    faces = model.detect_faces(pixels)
    # skip cases where we could not detect a face
    if len(faces) == 0:
        return None
    # extract details of the face
    x1, y1, width, height = faces[0]['box']
    # force detected pixel values to be positive (bug fix)
    x1, y1 = abs(x1), abs(y1)
    # convert into coordinates
    x2, y2 = x1 + width, y1 + height
    # retrieve face pixels
    face_pixels = pixels[y1:y2, x1:x2]
    # resize pixels to the DCGAN size
    image = Image.fromarray(face_pixels)
    image = image.resize(required_size)
    #face_array = asarray(image)
    return image


# load images and extract faces for all images in a directory
def load_faces(directory, dest_dir, n_faces):
    # prepare DCGAN
    model = MTCNN()
    faces = list()
    # enumerate files
    for filename in listdir(directory):
        # load the image
        pixels = load_image(directory + filename)
        # get face
        face_img = extract_face(model, pixels)
        if face_img is None:
            continue

        face_img.save(dest_dir + filename, "JPEG")

        # # store
        # faces.append(face)
        # print(len(faces), face.shape)
        # # stop once we have enough
        # if len(faces) >= n_faces:
        #     break

        # save to file instead of np array
        if len(faces) >= n_faces:
            break


    #return asarray(faces)


# directory that contains all images
directory = '../data/CELEB/img_align_celeba/'
dest_dir = '../data/CELEB_cropped/img_align_celeba/'
# load and extract all faces
load_faces(directory, dest_dir, 4)
#print('Loaded: ', all_faces.shape)
# save in compressed format
#savez_compressed('img_align_celeba.npz', all_faces)
