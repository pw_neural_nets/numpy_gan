SEED = 420
import random
import numpy as np
import torch
import torchvision.utils as vutils
import matplotlib.pyplot as plt

from gan_utils import data_loading
import hparams
from DCGAN import models
from DCGAN import pipeline

np.random.seed(SEED)
random.seed(SEED)
torch.manual_seed(SEED)

if __name__ == "__main__":
    device = torch.device("cuda:0" if (torch.cuda.is_available() and 1 > 0) else "cpu")

    data_loader = data_loading.get_data_loader(
        loader_type=hparams.loader_type,
        data_path=hparams.data_path,
        bs=hparams.bs,
        image_size=hparams.image_size
    )

    generator = models.Generator(channels_shapes=hparams.gen_channels_shapes).to(device)
    discriminator = models.Discriminator(channels_shapes=hparams.disc_channels_shapes).to(device)

    generator.apply(models.weights_init)
    discriminator.apply(models.weights_init)

    gen_optimizer = torch.optim.Adam(generator.parameters(), lr=hparams.gen_lr, betas=hparams.betas)
    disc_optimizer = torch.optim.Adam(discriminator.parameters(), lr=hparams.disc_lr, betas=hparams.betas)

    gen_loss_fn = torch.nn.BCELoss()
    disc_loss_fn = torch.nn.BCELoss()

    dcgan = pipeline.DCGAN(
        generator=generator,
        discriminator=discriminator,
        gen_loss_fn=gen_loss_fn,
        disc_loss_fn=disc_loss_fn,
        gen_optimizer=gen_optimizer,
        disc_optimizer=disc_optimizer,
        data_loader=data_loader,
        device=device,
    )

    dcgan.train_model()

    plt.figure(figsize=(10, 5))
    plt.title("Generator and Discriminator Loss During Training")
    plt.plot(dcgan.gen_losses, label="G")
    plt.plot(dcgan.disc_losses, label="D")
    plt.xlabel("iterations")
    plt.ylabel("Loss")
    plt.legend()
    plt.show()

    # Grab a batch of real images from the dataloader
    real_batch = next(iter(data_loader))

    # Plot the real images
    plt.figure(figsize=(15, 15))
    plt.subplot(1, 2, 1)
    plt.axis("off")
    plt.title("Real Images")
    plt.imshow(
        np.transpose(vutils.make_grid(
            real_batch[0].to(device)[:128],
            padding=5,
            normalize=True
        ).cpu(), (1, 2, 0)))

    # Plot the fake images from the last epoch
    plt.subplot(1, 2, 2)
    plt.axis("off")
    plt.title("Fake Images")
    plt.imshow(np.transpose(dcgan.img_list[-5], (1, 2, 0)))
    plt.show()
