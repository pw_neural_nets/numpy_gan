# numpy_gan

GAN architecture implemented only using numpy based on [paper](https://arxiv.org/abs/1506.03365)

[papers with code](https://paperswithcode.com/sota/image-generation-on-lsun-bedroom-256-x-256)

Datasets:
- [LSUN Bedroom](https://www.yf.io/p/lsun)
- [MNIST](http://yann.lecun.com/exdb/mnist/)