import numpy as np
import torch
from torchvision import datasets, transforms
from skimage.transform import resize
from torch.utils import data as tdataset

import hparams

loader_types = {}


def register(func):
    loader_types[func.__name__] = func
    return func


class ImageTensorDataset(tdataset.Dataset):
    def __init__(self, data, labels, transform=None):
        self.data = data
        self.labels = labels
        self.transform = transform

    def __getitem__(self, index):
        x, y = self.data[index], self.labels[index]

        if self.transform:
            x = self.transform(x)

        return x.float(), y

    def __len__(self):
        return self.data.shape[0]


class ResizeNumpyImg:
    def __init__(self, image_size):
        self.img_size = image_size

    def __call__(self, x):
        return resize(x, (self.img_size, self.img_size))


@register
def load_MNIST(data_path, bs, image_size):
    transform = transforms.Compose([
        transforms.Resize(image_size),
        transforms.CenterCrop(image_size),
        transforms.ToTensor(),
        transforms.Normalize([hparams.loading_normalization_mean], [hparams.loading_normalization_var]),
    ])

    loader = tdataset.DataLoader(
        datasets.MNIST(
            data_path,
            download=True,
            transform=transform,
        ),
        batch_size=bs,
        shuffle=True)

    return loader


@register
def load_CELEB(data_path, bs, image_size):
    dataset = datasets.ImageFolder(
        root=data_path,
        transform=transforms.Compose([
            transforms.Resize(image_size),
            transforms.CenterCrop(image_size),
            transforms.ToTensor(),
            transforms.Normalize(3*[hparams.loading_normalization_mean], 3*[hparams.loading_normalization_var]),
        ])
    )

    loader = tdataset.DataLoader(
        dataset,
        batch_size=bs,
        shuffle=True,
        num_workers=1
    )
    return loader


@register
def load_cropped_CELEB(data_path, bs, image_size):
    dataset = datasets.ImageFolder(
        root=data_path,
        transform=transforms.Compose([
            transforms.Resize(image_size),
            transforms.CenterCrop(image_size),
            transforms.ToTensor(),
            transforms.Normalize(3*[hparams.loading_normalization_mean], 3*[hparams.loading_normalization_var]),
        ])
    )

    loader = tdataset.DataLoader(
        dataset,
        batch_size=bs,
        shuffle=True,
        num_workers=1
    )
    return loader


@register
def load_np_cropped_CELEB(data_path, bs, image_size):
    faces = np.load(data_path + '/CELEB/img_align_celeba.npz')['arr_0']

    labels = torch.from_numpy(np.zeros(faces.shape[0])).float()
    cropped_faces_ds = ImageTensorDataset(
        data=faces,
        labels=labels,
        transform=transforms.Compose([
            ResizeNumpyImg(image_size),
            transforms.ToTensor(),
            transforms.Normalize(3*[hparams.loading_normalization_mean], 3*[hparams.loading_normalization_var]),
        ]),
    )

    loader = tdataset.DataLoader(
        cropped_faces_ds,
        batch_size=bs,
        shuffle=True,
        num_workers=1
    )
    return loader


def get_data_loader(loader_type, **kwargs):
    if loader_type not in loader_types.keys():
        raise ValueError(f'{loader_type} is not a valid load option. Choose one of {loader_types.keys()}')
    return loader_types[loader_type](**kwargs)
