SEED = 420
import random
import itertools
import numpy as np
import torch

from gan_utils import data_loading
import hparams
from DCGAN import models
from DCGAN import pipeline

np.random.seed(SEED)
random.seed(SEED)
torch.manual_seed(SEED)

if __name__ == "__main__":
    device = torch.device("cuda:0" if (torch.cuda.is_available() and 1 > 0) else "cpu")

    data_loader = data_loading.get_data_loader(
        loader_type=hparams.loader_type,
        data_path=hparams.data_path,
        bs=hparams.bs,
        image_size=hparams.image_size
    )

    generator = models.InfoGenerator(channels_shapes=hparams.gen_channels_shapes).to(device)
    discriminator = models.InfoDiscriminator(channels_shapes=hparams.disc_channels_shapes).to(device)

    generator.apply(models.weights_init)
    discriminator.apply(models.weights_init)

    gen_optimizer = torch.optim.Adam(generator.parameters(), lr=hparams.gen_lr, betas=hparams.betas)
    disc_optimizer = torch.optim.Adam(discriminator.parameters(), lr=hparams.disc_lr, betas=hparams.betas)
    info_optimizer = torch.optim.Adam(
        itertools.chain(discriminator.parameters(), generator.parameters()), lr=hparams.disc_lr, betas=hparams.betas)

    gen_loss_fn = torch.nn.BCELoss()
    disc_loss_fn = torch.nn.BCELoss()
    cls_loss = torch.nn.CrossEntropyLoss()
    code_loss = torch.nn.MSELoss()

    info_gan = pipeline.InfoGAN(
        generator=generator,
        discriminator=discriminator,
        gen_loss_fn=gen_loss_fn,
        disc_loss_fn=disc_loss_fn,
        gen_optimizer=gen_optimizer,
        disc_optimizer=disc_optimizer,
        data_loader=data_loader,
        info_optimizer=info_optimizer,
        cls_loss=cls_loss,
        code_loss=code_loss,
        device=device,
    )

    info_gan.train_model()
